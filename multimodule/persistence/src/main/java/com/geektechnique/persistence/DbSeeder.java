package com.geektechnique.persistence;

import com.geektechnique.domain.Hotel;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DbSeeder implements CommandLineRunner {

    private HotelRepository hotelRepository;

    public DbSeeder(HotelRepository hotelRepository){
        this.hotelRepository = hotelRepository;
    }


    @Override
    public void run(String... args) throws Exception {
        Hotel mariot = new Hotel("mariot", 4, true);
        Hotel crazy8 = new Hotel( "crazy8", 2, true);

        List<Hotel> hotels = new ArrayList<>();
        hotels.add(mariot);
        hotels.add(crazy8);

        //this.hotelRepository.save(hotels); not sure why this throws errors
    }
}
